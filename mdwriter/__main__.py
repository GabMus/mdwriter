from pathlib import Path
import sys
from gi.repository import Gtk, Gio, GLib
from mdwriter.confManager import ConfManager
from mdwriter.app_window import AppWindow
from mdwriter.editor_view import EditorView
from mdwriter.preferences_window import PreferencesWindow
from mdwriter.base_app import BaseApp, AppAction


class GApplication(BaseApp):
    args = []

    def __init__(self):
        self.confman = ConfManager()
        super().__init__(
            app_id='org.gabmus.mdwriter',
            app_name='MD Writer',
            app_actions=[
                AppAction(
                    name='preferences',
                    func=self.show_preferences_window,
                    accel='<Primary>comma'
                ),
                AppAction(
                    name='shortcuts',
                    func=self.show_shortcuts_window,
                    accel='<Primary>question'
                ),
                AppAction(
                  name='about',
                  func=self.show_about_dialog
                ),
                AppAction(
                    name='quit',
                    func=lambda *_: self.quit(),
                    accel='<Primary>q'
                ),
                AppAction(
                    name='closetab',
                    func=lambda *_: self.close_tab(),
                    accel='<Primary>w'
                ),
                AppAction(
                    name='save',
                    func=lambda *_: self.save(),
                    accel='<Primary>s'
                ),
                AppAction(
                    name='saveas',
                    func=lambda *_: self.saveas(),
                    accel='<Primary><Shift>s'
                ),
                AppAction(
                    name='open',
                    func=lambda *_: self.open_document(),
                    accel='<Primary>o'
                ),
                AppAction(
                    name='new',
                    func=lambda *_: self.new_document(),
                    accel='<Primary>n'
                ),
                AppAction(
                    name='preview_mode',
                    func=self.preview_mode_changed,
                    stateful=True,
                    state_type=AppAction.StateType.RADIO,
                    state_default=self.confman.conf['preview_mode']
                )
            ],
            flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
            css_resource='/org/gabmus/mdwriter/ui/gtk_style.css'
        )

    def preview_mode_changed(
            self, action: Gio.SimpleAction, target: GLib.Variant, *_
    ):
        action.change_state(target)
        target_s = str(target).strip("'")
        if target_s not in ('vertical', 'horizontal', 'disabled'):
            target_s = 'vertical'
        self.confman.conf['preview_mode'] = target_s
        self.confman.save_conf()
        for win in self.get_windows():
            if isinstance(win, AppWindow):
                for tab in win.app_content.tabview.get_pages():
                    editor = tab.get_child()
                    if isinstance(editor, EditorView):
                        editor.set_preview_mode(target_s)

    def quit(self, *_):
        self.confman.trim_recents()
        self.confman.save_conf()
        for win in self.get_windows():
            win.close()

    def close_tab(self):
        win = self.get_active_window()
        if isinstance(win, AppWindow):
            win.close_tab()

    def save(self):
        win = self.get_active_window()
        if isinstance(win, AppWindow):
            win.save()

    def saveas(self):
        win = self.get_active_window()
        if isinstance(win, AppWindow):
            win.saveas()

    def open_document(self):
        win = self.get_active_window()
        if isinstance(win, AppWindow):
            win.open_document()

    def new_document(self):
        win = self.get_active_window()
        if isinstance(win, AppWindow):
            win.new_document()

    def show_about_dialog(self, *_):
        dialog = Gtk.Builder.new_from_resource(
            '/org/gabmus/mdwriter/aboutdialog.ui'
        ).get_object('aboutdialog')
        dialog.set_modal(True)
        dialog.set_transient_for(self.get_active_window())
        dialog.present()

    def show_shortcuts_window(self, *_):
        shortcuts_win = Gtk.Builder.new_from_resource(
            '/org/gabmus/mdwriter/ui/shortcutsWindow.ui'
        ).get_object('shortcuts-win')
        shortcuts_win.props.section_name = 'shortcuts'
        shortcuts_win.set_transient_for(self.get_active_window())
        shortcuts_win.set_modal(True)
        shortcuts_win.present()

    def show_preferences_window(self, *_):
        preferences_win = PreferencesWindow()
        preferences_win.set_transient_for(self.get_active_window())
        preferences_win.set_modal(True)
        preferences_win.present()

    def do_activate(self):
        super().do_activate()
        win = None
        for w in self.get_windows():
            if isinstance(w, AppWindow):
                win = w
                break
        if win is None:
            win = AppWindow()
            self.add_window(win)
        win.present()
        assert(isinstance(win, AppWindow))
        for arg in self.args[1:]:
            p = Path(arg)
            if p.is_dir():
                continue
            win.app_content.open_file(p)

    def do_command_line(self, args: Gio.ApplicationCommandLine):
        """
        GTK.Application command line handler
        called if Gio.ApplicationFlags.HANDLES_COMMAND_LINE is set.
        must call the self.do_activate() to get the application up and running.
        """
        self.args = args.get_arguments()
        self.do_activate()
        return 0


def main():

    application = GApplication()

    try:
        ret = application.run(sys.argv)
    except SystemExit as e:
        ret = e.code

    sys.exit(ret)


if __name__ == '__main__':
    main()
