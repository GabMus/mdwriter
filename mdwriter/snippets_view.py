from gettext import gettext as _
from typing import Optional
from gi.repository import Adw, GObject, Gtk, GtkSource
from mdwriter.action_helper import new_action, new_action_group
from mdwriter.confManager import ConfManager
from mdwriter.snippet_store import Snippet, SnippetStore


@Gtk.Template(resource_path='/org/gabmus/mdwriter/ui/snippet_list_row.ui')
class SnippetListRow(Gtk.ListBoxRow):
    __gtype_name__ = 'SnippetListRow'
    __gsignals__ = {
        'snip-delete': (
            GObject.SignalFlags.RUN_LAST, None, (str,)
        ),
        'snip-edit': (
            GObject.SignalFlags.RUN_LAST, None, (str,)
        )
    }
    name_label: Gtk.Label = Gtk.Template.Child()

    def __init__(self, snippet: Snippet):
        super().__init__()
        self.snippet = snippet
        self.snippet.connect('notify::name', self.on_snippet_name_changed)
        self.on_snippet_name_changed()
        new_action_group(self, 'snippetrow', [
            new_action('delete', self.action_delete),
            new_action('edit', self.action_edit),
        ])

    def action_delete(self, *_):
        self.emit('snip-delete', '')

    def action_edit(self, *_):
        self.emit('snip-edit', '')

    def on_snippet_name_changed(self, *_):
        self.name_label.set_text(self.snippet.name)


@Gtk.Template(resource_path='/org/gabmus/mdwriter/ui/snippets_view.ui')
class SnippetsView(Gtk.Box):
    __gtype_name__ = 'SnippetsView'
    __gsignals__ = {
        'snippet-selected': (
            GObject.SignalFlags.RUN_LAST,
            None,
            (GObject.Object,)
        )
    }
    stack: Gtk.Stack = Gtk.Template.Child()
    listbox: Gtk.ListBox = Gtk.Template.Child()
    listbox_sw: Gtk.ScrolledWindow = Gtk.Template.Child()
    empty_state: Adw.StatusPage = Gtk.Template.Child()

    def __init__(self):
        super().__init__()
        self.confman = ConfManager()
        self.snippet_store = SnippetStore.get_instance()
        self.listbox.bind_model(self.snippet_store, self.__create_row)
        self.snippet_store.connect('items-changed', self.on_items_changed)
        self.on_items_changed()

    @Gtk.Template.Callback()
    def on_row_activated(self, _, row: SnippetListRow):
        self.emit('snippet-selected', row.snippet)

    @Gtk.Template.Callback()
    def on_add_btn_clicked(self, *__):
        self.spawn_snippet_dialog()

    def spawn_snippet_dialog(self, to_edit: Optional[Snippet] = None):
        dialog = Gtk.MessageDialog(
            transient_for=self.get_root(),
            modal=True,
            text=_('New Snippet') if to_edit is None else _('Edit Snippet'),
            buttons=Gtk.ButtonsType.NONE
        )
        dialog.add_button(
            _('Cancel'), Gtk.ResponseType.CANCEL
        ).get_style_context().add_class('destructive-action')
        dialog.add_button(
            _('Save'), Gtk.ResponseType.OK
        ).get_style_context().add_class('suggested-action')

        dialog.name_entry = Gtk.Entry(
            placeholder_text=_('Name...')
        )
        dialog.code_sourcebuf = GtkSource.Buffer(
            style_scheme=(
                GtkSource.StyleSchemeManager.get_default().get_scheme(
                    self.confman.conf['editor_theme']
                ) or GtkSource.StyleSchemeManager.get_default().get_scheme(
                    'Adwaita'
                )
            )
        )
        dialog.code_sourceview = GtkSource.View(
            buffer=dialog.code_sourcebuf,
            hexpand=True, vexpand=True,
            top_margin=6, bottom_margin=6, left_margin=6, right_margin=6,
            tab_width=2, indent_width=2, insert_spaces_instead_of_tabs=True
        )
        if self.confman.conf['editor_font_enabled']:
            font = self.confman.conf['editor_font']
            family = ' '.join(font.split(' ')[:-1])
            size = str(max(int(font.split(' ')[-1]), 3))
            dialog.style_provider = Gtk.CssProvider()
            dialog.style_provider.load_from_data(
                f'''.sourceview {{
                        font-family: {family}, monospace;
                        font-size: {size}pt;
                }}'''.encode()
            )
            dialog.code_sourceview.get_style_context().add_provider(
                dialog.style_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION,
            )
        for c in ('frame', 'card'):
            dialog.code_sourceview.get_style_context().add_class(c)
        dialog.code_sw = Gtk.ScrolledWindow(
            hscrollbar_policy=Gtk.PolicyType.NEVER,
            width_request=280, height_request=200
        )
        dialog.code_sw.set_child(dialog.code_sourceview)

        dialog.data_box = Gtk.Box(
            orientation=Gtk.Orientation.VERTICAL, spacing=6,
            margin_top=12, margin_bottom=12, margin_start=12, margin_end=12
        )
        dialog.data_box.append(dialog.name_entry)
        dialog.data_box.append(dialog.code_sw)

        dialog.get_content_area().append(
            dialog.data_box
        )

        if to_edit is not None:
            dialog.name_entry.set_text(to_edit.name)
            dialog.code_sourcebuf.set_text(to_edit.text)

        def on_response(_dialog, res):
            if res == Gtk.ResponseType.OK:
                name = _dialog.name_entry.get_text().strip()
                text = _dialog.code_sourcebuf.get_text(
                    _dialog.code_sourcebuf.get_start_iter(),
                    _dialog.code_sourcebuf.get_end_iter(),
                    True
                )
                if name and text:
                    if to_edit is not None:
                        to_edit.name = name
                        to_edit.text = text
                        self.snippet_store.invalidate_sort()
                        self.snippet_store.dump_to_conf()
                    else:
                        self.snippet_store.add_snippet(Snippet(
                            name=name, text=text
                        ))
                    _dialog.close()
            elif res == Gtk.ResponseType.CANCEL:
                _dialog.close()

        dialog.connect('response', on_response)
        dialog.present()

    def __create_row(self, snippet: Snippet, *_) -> SnippetListRow:
        row = SnippetListRow(snippet)
        row.connect('snip-delete', self.on_snippet_delete)
        row.connect('snip-edit', self.on_snippet_edit)
        return row

    def on_snippet_delete(self, row: SnippetListRow, _):
        self.snippet_store.remove_snippet(row.snippet.name)

    def on_snippet_edit(self, row: SnippetListRow, _):
        self.spawn_snippet_dialog(to_edit=row.snippet)

    def on_items_changed(self, *_):
        self.stack.set_visible_child(
            self.listbox_sw if self.snippet_store.get_n_items() > 0
            else self.empty_state
        )
