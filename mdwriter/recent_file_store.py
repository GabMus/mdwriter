from pathlib import Path
from gi.repository import GObject, Gio, Gtk
from datetime import datetime
from mdwriter.confManager import ConfManager


class RecentFile(GObject.Object):
    def __init__(self, path: Path, last_modified: datetime):
        self.__path = path
        self.__last_modified = last_modified
        super().__init__()

    @GObject.Property()
    def path(self) -> Path:
        return self.__path

    @path.setter
    def path(self, n_path: Path):
        self.__path = n_path

    @GObject.Property()
    def last_modified(self) -> datetime:
        return self.__last_modified

    @last_modified.setter
    def last_modified(self, n_last_modified: datetime):
        self.__last_modified = n_last_modified

    @classmethod
    def from_dict(cls, d: dict):
        assert('path' in d.keys() and 'last_modified' in d.keys())
        return cls(Path(d['path']), datetime.fromtimestamp(d['last_modified']))

    def to_dict(self) -> dict:
        return {
            'path': str(self.path),
            'last_modified': self.last_modified.timestamp()
        }


class RecentFileStore(Gtk.SortListModel):
    def __init__(self):
        self.confman = ConfManager()

        self.sorter = Gtk.CustomSorter()
        self.sorter.set_sort_func(self.__sort_func)
        self.list_store = Gio.ListStore(item_type=RecentFile)
        super().__init__(model=self.list_store, sorter=self.sorter)
        self.populate()

    def __sort_func(self, f1: RecentFile, f2: RecentFile, *_) -> int:
        return -1 if f1.last_modified > f2.last_modified else 1

    def invalidate_sort(self):
        self.sorter.set_sort_func(self.__sort_func)

    def empty(self):
        self.list_store.remove_all()

    def populate(self):
        self.empty()
        to_rm = []
        for rfd in self.confman.conf['recent_files'].values():
            rf = RecentFile.from_dict(rfd)
            if not rf.path.is_file():
                to_rm.append(str(rf.path))
            else:
                self.list_store.append(rf)
        if len(to_rm) > 0:
            for p in to_rm:
                self.confman.conf['recent_files'].pop(p)
            self.confman.save_conf()

    def add_rf(self, rf: RecentFile):
        exists = False
        for erf in self.list_store:
            if erf.path == rf.path:
                exists = True
                erf.last_modified = rf.last_modified
                self.invalidate_sort()
        if not exists:
            self.list_store.append(rf)
        self.confman.conf['recent_files'][str(rf.path)] = {
            'path': str(rf.path),
            'last_modified': datetime.now().timestamp()
        }
        self.confman.save_conf()
