from gi.repository import GtkSource


__MD_CSS = '''
body, html {{
    background-color: {0};
    color: {1};
}}
blockquote {{
    border-left: 5px solid {2};
    margin-left: 0;
    padding-left: 15px;
}}
'''


def build_css(sourcebuf: GtkSource.Buffer) -> str:
    scheme = sourcebuf.get_style_scheme()
    text = scheme.get_style('text')
    comment = scheme.get_style('def:comment')
    return __MD_CSS.format(
        text.props.background,
        text.props.foreground,
        comment.props.foreground
    )
