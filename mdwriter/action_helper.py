from typing import Callable, List, Optional
from gi.repository import Gio, Gtk


def new_action(
        name: str, func: Callable, accel: Optional[str] = None
) -> Gio.SimpleAction:
    action = Gio.SimpleAction.new(name, None)
    action.connect('activate', func)
    action._custom_accel = accel
    return action


def new_action_group(
        parent: Gtk.Widget, name: str, actions: List[Gio.SimpleAction],
        shortcut_controller: Optional[Gtk.ShortcutController] = None
):
    if shortcut_controller is None:
        shortcut_controller = parent
    action_group = Gio.SimpleActionGroup()
    # shorctut_controller = Gtk.ShortcutController()
    for action in actions:
        action_group.add_action(action)
        # TODO
        if (
                hasattr(action, '_custom_accel') and
                action._custom_accel is not None
        ):
            action._shortcut = Gtk.Shortcut(
                trigger=Gtk.ShortcutTrigger.parse_string(
                    action._custom_accel
                ),
                action=Gtk.ShortcutAction.parse_string(
                    f'action({name}.{action.get_name()})'
                )
            )
            shortcut_controller.add_shortcut(action._shortcut)
    parent._action_group = action_group
    parent.insert_action_group(name, parent._action_group)
